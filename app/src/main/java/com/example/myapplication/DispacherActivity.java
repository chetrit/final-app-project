package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class DispacherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispacher);

        //open target activity
        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.SHARED_FILE_NAME, MODE_PRIVATE);
        String lastActivityClassName = sharedPreferences.getString(MainActivity.LAST_ACTIVITY_KEYNAME, MainActivity.class.getName());

        Class<?> activityClass;
        try {
            activityClass = Class.forName(lastActivityClassName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            activityClass = MainActivity.class;
        }

        startActivity(new Intent(this, activityClass));
    }
}