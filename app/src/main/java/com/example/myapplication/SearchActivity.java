package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.adapters.ItemDataAdapter;
import com.example.myapplication.model.Item;
import com.example.myapplication.model.ItemData;
import com.example.myapplication.retrofit.ApiInterface;
import com.example.myapplication.retrofit.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    ApiInterface apiInterface;
    RecyclerView itemRecyclerView;
    ItemDataAdapter itemDataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        SharedPreferences sharedPreferencesFile= getSharedPreferences(MainActivity.SHARED_FILE_NAME, MODE_PRIVATE);
        String searchStr = sharedPreferencesFile.getString(MainActivity.SEARCH_KEYNAME, null);

//        Intent intent = getIntent();
//        String searchStr = intent.getStringExtra(MainActivity.SEARCH_KEYNAME);
        this.makeRequest(searchStr);
    }

    private void makeRequest(String searchStr) {
        apiInterface = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);
        Call<ItemData> call = apiInterface.getAllData("stackoverflow", searchStr);
        call.enqueue((new Callback<ItemData>() {

            @Override
            public void onResponse(Call<ItemData> call, Response<ItemData> response) {

                ItemData itemDataList = response.body();
                if(itemDataList.getItems().size() == 0 || itemDataList == null) {
                    Toast.makeText(SearchActivity.this, "incorrect keyword search", Toast.LENGTH_SHORT).show();
                }
//                Toast.makeText(SearchActivity.this, "Server call is Working", Toast.LENGTH_SHORT).show();
                getAllItemData(itemDataList.getItems());
            }

            @Override
            public void onFailure(Call<ItemData> call, Throwable t) {
                Toast.makeText(SearchActivity.this, "call is not ok", Toast.LENGTH_SHORT).show();

            }
        }));
    }

    private void getAllItemData(List<Item> listItem) {
        itemRecyclerView = findViewById(R.id.itemDataRecyclerView);
        itemDataAdapter = new ItemDataAdapter(this, listItem);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false );
        itemRecyclerView.setLayoutManager(layoutManager);
        itemRecyclerView.setAdapter(itemDataAdapter);
    }

    public void goToSearchActivity(View view) {
        Intent intent = new Intent(this, HistoryActivity.class);

        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //save data
        SharedPreferences sharedPreferencesFile = getSharedPreferences(MainActivity.SHARED_FILE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferencesFile.edit();

        editor.putString(MainActivity.LAST_ACTIVITY_KEYNAME, getClass().getName()); // Put class name of the current activity

        editor.commit();

    }
}