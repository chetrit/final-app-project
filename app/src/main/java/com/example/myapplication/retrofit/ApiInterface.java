package com.example.myapplication.retrofit;

import com.example.myapplication.model.ItemData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("search/?")
    Call<ItemData> getAllData(
            @Query("site") String site,
            @Query("tagged") String tagged
    );
}
