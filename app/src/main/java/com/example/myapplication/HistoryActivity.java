package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.helper.DataBaseHelper;
import com.example.myapplication.model.SearchHistoryModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

public class HistoryActivity extends AppCompatActivity implements OnMapReadyCallback {
    SupportMapFragment supportMapFragment;
    FusedLocationProviderClient client;
    double longitude;
    double latitude;
    GoogleMap map;
    ListView lv_customerList;
    DataBaseHelper dataBaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        lv_customerList = findViewById(R.id.lv_customerList);
        dataBaseHelper = new DataBaseHelper(HistoryActivity.this);
        showCustomersOnListView(dataBaseHelper);
        this.setOnclickItem();
        supportMapFragment.getMapAsync(this);
    }

    private void setOnclickItem() {
        lv_customerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SearchHistoryModel searchHistoryModel = (SearchHistoryModel)parent.getItemAtPosition(position);
//                dataBaseHelper.deleteOne(searchHistoryModel);
//                showCustomersOnListView(dataBaseHelper);
                LatLng latLng = new LatLng(Double.parseDouble(searchHistoryModel.getLatitude()),
                        Double.parseDouble(searchHistoryModel.getLongitude()));
                map.addMarker(new MarkerOptions().position(latLng).title(searchHistoryModel.getAddress()));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
//                Toast.makeText(HistoryActivity.this, "Deleted " + searchHistoryModel.getLatitude(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);
        LatLng myAdress = new LatLng(19.169257, 73.341601);
        map.addMarker(new MarkerOptions().position(myAdress).title("mitza"));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15));
    }

    public void changeLocation(View view) {
        map.addMarker(new MarkerOptions().position(new LatLng(-22.86789171982668, -43.46314018783556)).title("mitza"));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-22.86789171982668, -43.46314018783556), 15));
    }

    //SQL method
    private void showCustomersOnListView(DataBaseHelper dataBaseHelper) {
        ArrayAdapter customerArrayAdapter = new ArrayAdapter<SearchHistoryModel>(HistoryActivity.this, android.R.layout.simple_list_item_1, dataBaseHelper.getEveryone());
        lv_customerList.setAdapter(customerArrayAdapter);
    }

    public void goToAboutActivity(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    protected void onPause() {
        super.onPause();
        //save data
        SharedPreferences sharedPreferencesFile = getSharedPreferences(MainActivity.SHARED_FILE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferencesFile.edit();

        editor.putString(MainActivity.LAST_ACTIVITY_KEYNAME, getClass().getName()); // Put class name of the current activity

        editor.commit();

    }
}