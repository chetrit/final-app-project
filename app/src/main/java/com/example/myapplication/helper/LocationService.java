package com.example.myapplication.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.List;
import java.util.Locale;

public class LocationService extends AppCompatActivity {
    FusedLocationProviderClient client;
    Location network_loc;
    Location final_loc;
    double longitude;
    double latitude;
    String userCountry, userAddress;
    LocationManager locationManager;
    private static LocationService single_instance = null;
    private Context context;

    private LocationService(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static LocationService getInstance(Bundle savedInstanceState) {
        if (single_instance == null)
            single_instance = new LocationService(savedInstanceState);

        return single_instance;
    }

    public void initalCurrentLocation() {
//        this.client = LocationServices.getFusedLocationProviderClient(this);
        this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        try {
            this.network_loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (network_loc != null) {
            this.final_loc = network_loc;
            this.latitude = final_loc.getLatitude();
            this.longitude = final_loc.getLongitude();
        }
        else {
            this.latitude = 0.0;
            this.longitude = 0.0;
        }

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                this.userCountry = addresses.get(0).getCountryName();
                this.userAddress = addresses.get(0).getAddressLine(0);
            }
            else {
                this.userCountry = "Unknown";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getAddress() {
        return this.userCountry + " " + this.userAddress;
    }
}
