package com.example.myapplication.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.myapplication.model.CustomerModel;
import com.example.myapplication.model.SearchHistoryModel;

import java.util.ArrayList;
import java.util.List;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String SEARCH_HISTORY_TABLE = "SEARCH_HISTORY_TABLE";
    public static final String COLUMN_SEARCH_KEYWORD = "SEARCH_KEYWORD";
    public static final String COLUMN_SEARCH_ADDRESS = "SEARCH_ADDRESS";
    public static final String COLUMN_SEARCH_LATITUDE = "SEARCH_LATITUDE";
    public static final String COLUMN_SEARCH_LONGTITUDE = "SEARCH_LONGTITUDE";
    public static final String COLUMN_ID = "ID";

    public DataBaseHelper(@Nullable Context context) {
        super(context, "search.db", null, 1);
    }

    //this is called in first time a database is accessed. There should be code in here to create a new database/
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableStatment = "CREATE TABLE " + SEARCH_HISTORY_TABLE + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_SEARCH_KEYWORD + " TEXT, " + COLUMN_SEARCH_ADDRESS + " TEXT, " + COLUMN_SEARCH_LATITUDE + " TEXT, " + COLUMN_SEARCH_LONGTITUDE + " TEXT)";

        db.execSQL(createTableStatment);

    }

    // this is called if the database version number changes. It prevent previous users apps from breaking when you change the database design/
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean addOne(SearchHistoryModel searchHistoryModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_SEARCH_ADDRESS, searchHistoryModel.getAddress());
        cv.put(COLUMN_SEARCH_KEYWORD, searchHistoryModel.getSearchKeyword());
        cv.put(COLUMN_SEARCH_LATITUDE, searchHistoryModel.getLatitude());
        cv.put(COLUMN_SEARCH_LONGTITUDE, searchHistoryModel.getLongitude());


        long insert = db.insert(SEARCH_HISTORY_TABLE, null, cv);
        if(insert == -1) {
            return false;
        } else {
        return true;
        }
    }

    public boolean deleteOne(SearchHistoryModel searchHistoryModel) {
        // find customerModel in the database. if it found' delete it and return true.
        // if it is not found, return false.

        SQLiteDatabase db = this.getWritableDatabase();
        String queryString = "DELETE FROM " + SEARCH_HISTORY_TABLE + " WHERE " + COLUMN_ID + " = " + searchHistoryModel.getId();

        Cursor cursor = db.rawQuery(queryString, null);

        if(cursor.moveToFirst()) {
            return true;
        } else {
            return false;
        }
    }

    public List<SearchHistoryModel> getEveryone() {
        List<SearchHistoryModel> returnList = new ArrayList<SearchHistoryModel>();

        // get data from the database
        String queryString ="SELECT * FROM " + SEARCH_HISTORY_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(queryString, null);

        if(cursor.moveToFirst()) {
            // loop through the cursor (result set) and create new customer objects. Put then into the return list
            do {
                int searchID = cursor.getInt(0);
                String searchKeyword = cursor.getString(1);
                String searchAddress = cursor.getString(2);
                String searchLatitude = cursor.getString(3);
                String searchLongtitude = cursor.getString(4);

                SearchHistoryModel newSearch = new SearchHistoryModel(searchID, searchKeyword, searchAddress, searchLatitude, searchLongtitude);
                returnList.add((newSearch));

            }while (cursor.moveToNext() );

        } else {
            // failure. do not add anything to the list
        }
        cursor.close();;
        db.close();
        return returnList;
    }
}
