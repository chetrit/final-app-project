package com.example.myapplication;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.myapplication.helper.DataBaseHelper;

import com.example.myapplication.model.SearchHistoryModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    public final static String SEARCH_KEYNAME = "com.example.MyApplication.MainActivity.SEARCH_KEYNAME";
    public final static String SHARED_FILE_NAME = "com.example.MyApplication.MainActivity.SharedFile1";
    public final static String LAST_ACTIVITY_KEYNAME = "com.example.MyApplication.MainActivity.lastactivity";
    EditText searchText;
    FusedLocationProviderClient client;
    Location network_loc;
    Location final_loc;
    double longitude;
    double latitude;
    String userCountry, userAddress;
    TextView tv;
    DataBaseHelper dataBaseHelper;
    boolean locationPermission = false;

    public MainActivity() {
        this.dataBaseHelper = new DataBaseHelper(MainActivity.this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.searchText = findViewById(R.id.search_text_input);
        locationPermission = this.checkPermission();
        client = LocationServices.getFusedLocationProviderClient(this);

        // show valus auto without enter new values
        SharedPreferences sharedPreferencesFile= getSharedPreferences(MainActivity.SHARED_FILE_NAME, MODE_PRIVATE);
        String searchStr = sharedPreferencesFile.getString(MainActivity.SEARCH_KEYNAME, null);
        searchText.setText(searchStr);
    }

    public void onSearch(View view) {
        String searchStr = searchText.getText().toString();

        if(searchStr.length() != 0) {
            this.getCurrentLocation();
            SearchHistoryModel searchHistoryModel = new SearchHistoryModel(-1, searchStr, userCountry + ", " + userAddress, String.valueOf(latitude), String.valueOf(longitude));
            boolean success = dataBaseHelper.addOne(searchHistoryModel);
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra(SEARCH_KEYNAME, searchStr);
            startActivity(intent);
        } else {
        Toast.makeText(MainActivity.this, "please enter an search input", Toast.LENGTH_SHORT).show();
        }
    }

    public void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        try {
            network_loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (network_loc != null) {
            final_loc = network_loc;
            latitude = final_loc.getLatitude();
            longitude = final_loc.getLongitude();
        }
        else {
            latitude = 0.0;
            longitude = 0.0;
        }

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                userCountry = addresses.get(0).getCountryName();
                userAddress = addresses.get(0).getAddressLine(0);
                tv.setText(userCountry + ", " + userAddress);
            }
            else {
                userCountry = "Unknown";
                tv.setText(userCountry);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkPermission(){
        if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION )
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE}, 1);
            return false;
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        //save data
        SharedPreferences sharedPreferencesFile = getSharedPreferences(SHARED_FILE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferencesFile.edit();

        editor.putString(SEARCH_KEYNAME, searchText.getText().toString()); //put message to shared file
        editor.putString(LAST_ACTIVITY_KEYNAME, getClass().getName()); // Put class name of the current activity

        editor.commit();

    }
}