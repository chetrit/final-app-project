package com.example.myapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.model.Item;

import java.util.List;

public class ItemDataAdapter extends RecyclerView.Adapter<ItemDataAdapter.ItemDataViewHolder> {
    private Context context;
    private List<Item> itemDataList;

    public ItemDataAdapter(Context context, List<Item> itemDataList) {
        this.context = context;
        this.itemDataList = itemDataList;
    }

    @NonNull
    @Override
    public ItemDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.data_recycler_items, parent, false);
        return new ItemDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemDataViewHolder holder, int position) {
        holder.dataItemTitle.setText(itemDataList.get(position).getTitle());
        holder.dataItemLink.setText(itemDataList.get(position).getLink());
        Glide.with(context).load(itemDataList.get(position).getOwner().getProfileImage()).into(holder.dataItemImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri siteUrl = Uri.parse(itemDataList.get(position).getLink());
                Intent intent = new Intent(Intent.ACTION_VIEW, siteUrl);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemDataList.size();
    }

    public static class ItemDataViewHolder extends RecyclerView.ViewHolder {
        ImageView dataItemImage;
        TextView dataItemLink, dataItemTitle;

        public ItemDataViewHolder(@NonNull View itemView) {
            super(itemView);

            dataItemImage = itemView.findViewById(R.id.data_item_image);
            dataItemLink = itemView.findViewById(R.id.data_item_link);
            dataItemTitle = itemView.findViewById(R.id.data_item_title);
        }
    }
}
