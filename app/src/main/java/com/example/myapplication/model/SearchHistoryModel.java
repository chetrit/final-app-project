package com.example.myapplication.model;

public class SearchHistoryModel {
    private int id;
    private String searchKeyword;
    private String address;
    private String latitude;
    private String longitude;

    public SearchHistoryModel(int id, String searchKeyword, String address, String latitude, String longitude) {
        this.id = id;
        this.searchKeyword = searchKeyword;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "SearchHistoryModel{" +
                "id=" + id +
                ", searchKeyword='" + searchKeyword + '\'' +
                ", address='" + address + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
