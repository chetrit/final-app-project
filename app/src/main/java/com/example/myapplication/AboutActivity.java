package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.VideoView;

public class AboutActivity extends AppCompatActivity {
    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        this.openInitVideo();
    }

    public void sendMessage(View view) {
        String messageStr = "hello i want to contact you ";
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        intent.setType("text/plain");
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, "yonibio100@gmail.com");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Message from user AppForer");
        intent.putExtra(Intent.EXTRA_TEXT, messageStr);

        startActivity(Intent.createChooser(intent, "Send Email"));

    }

    public void callMe(View view) {
        Uri number = Uri.parse("tel:0524671281");
        Intent intent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(intent);

    }

    private void openInitVideo() {
        String uriString = "android.resource://" + getPackageName() + "/" + R.raw.forer;

        Uri uriForer = Uri.parse(uriString);

        videoView = findViewById(R.id.videoViewForer);
        videoView.setVideoURI(uriForer);

        videoView.setMediaController(new MediaController(this));
        videoView.requestFocus();
        videoView.start();
    }

    protected void onPause() {
        super.onPause();

        //save data
        SharedPreferences sharedPreferencesFile = getSharedPreferences(MainActivity.SHARED_FILE_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferencesFile.edit();

        editor.putString(MainActivity.LAST_ACTIVITY_KEYNAME, getClass().getName()); // Put class name of the current activity

        editor.commit();

    }
}